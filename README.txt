ABOUT
==========
The 'Flickr public (Xpert)' repository plugin was developed by
    Barry Oosthuizen - barry.oosthuizen@nottingham.ac.uk

The code is heavily based on core Moodle's "Flickr public" repository plugin.

This module may be distributed under the terms of the General Public License
(see http://www.gnu.org/licenses/gpl.txt for details)

PURPOSE
==========
The 'Flickr public (Xpert)' repository allows a user to attribute an image returned via the Flickr API to the copyright holder.
This is accomplished by appending the copyright notice below the image itself.  

The size of the image (small, medium or large)
can also be specified.

INSTALLATION
==========
The 'Flickr public (Xpert)' repository follows the standard installation procedure.

1. Create folder <path to your moodle dir>/repository/xpert_flickr_public.
2. Extract files from folder inside archive to created folder.
3. Visit page Home ► Site administration ► Notifications to complete installation.
4. Create a new repository instance.  (You will need a Flickr API key)
