<?php
// This file is part of the Flickr public (Xpert) repository plugin for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Tests for Xpert Flickr Public repository generator
 *
 * @package    repository_xpert_flickr_public
 * @copyright  2014 onwards, University of Nottingham
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

/**
 * TXpert Flickr Public repository generator tests
 *
 * @package    repository_xpert_flickr_public
 * @copyright  2014 onwards, University of Nottingham
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class repository_xpert_flickr_publicgenerator_testcase extends advanced_testcase {

    /**
     * Basic test of creation of repository types.
     *
     * @group repository_xpert_flickr_public
     * @group repository_xpert_flickr_public_create_type
     * @group uon
     * @return void
     */
    public function test_create_type() {
        global $DB;
        $this->resetAfterTest(true);

        $type = 'xpert_flickr_public';

        // Enable the repository.
        $repotype = $this->getDataGenerator()->create_repository_type($type);
        $this->assertEquals($repotype->type, $type, 'Unexpected name after creating repository type ' . $type);
        $this->assertTrue($DB->record_exists('repository', array('type' => 'xpert_flickr_public', 'visible' => 1)));

        // Check that all the repositories have been enabled.
        $caughtexception = false;
        try {
            $this->getDataGenerator()->create_repository_type($type);
        } catch (repository_exception $e) {
            if ($e->getMessage() === 'This repository already exists') {
                $caughtexception = true;
            }
        }

        $this->assertTrue($caughtexception, "Repository type '$type' should have already been enabled");
    }

    /**
     * Ensure that the type options are properly saved.
     *
     * @group repository_xpert_flickr_public
     * @group repository_xpert_flickr_public_custom_options
     * @group uon
     * @global moodle_database $DB
     * @return void
     */
    public function test_create_type_custom_options() {
        global $DB;
        $this->resetAfterTest(true);

        // Single instances.
        // Note: for single instances repositories enablecourseinstances and enableuserinstances are forced set to 0.
        $record = new stdClass();
        $record->api_key = '12345';
        $record->debug = 0;
        $record->enableuserinstances = 0;
        $record->enablecourseinstances = 0;
        $record->pluginname = 'Custom xpert Flickr';

        $flickr = $this->getDataGenerator()->create_repository_type('xpert_flickr_public', $record);

        $config = get_config('xpert_flickr_public');

        $this->assertEquals($record, $config);

        $record = new stdClass();
        $record->name = 'Custom xpert Flickr';
        $record->useattribution = 1;

        $instance = $this->getDataGenerator()->create_repository('xpert_flickr_public', $record);

        $this->assertEquals('Custom xpert Flickr',
            $DB->get_field('repository_instances', 'name', array('typeid' => $flickr->id, 'id' => $instance->id), MUST_EXIST));
    }

    /**
     * Covers basic testing of instance creation.
     *
     * @group repository_xpert_flickr_public
     * @group repository_xpert_flickr_public_create_instance
     * @group uon
     * @return void
     */
    public function test_create_instance() {
        global $DB;
        $this->resetAfterTest(true);

        $course = $this->getDataGenerator()->create_course();
        $user = $this->getDataGenerator()->create_user();
        $block = $this->getDataGenerator()->create_block('online_users');

        $type = $this->getDataGenerator()->create_repository_type('xpert_flickr_public');

        $record = new stdClass();
        $record->name = 'A xpert_flickr_public instance';

        $instance = $this->getDataGenerator()->create_repository('xpert_flickr_public', $record);

        $this->assertEquals(2, $DB->count_records('repository_instances', array('typeid' => $type->id)));
        $this->assertEquals($record->name, $DB->get_field('repository_instances', 'name', array('id' => $instance->id)));

        // Course context.
        $record = new stdClass();
        $record->contextid = context_course::instance($course->id)->id;
        $instance = $this->getDataGenerator()->create_repository('xpert_flickr_public', $record);

        $this->assertEquals(3, $DB->count_records('repository_instances', array('typeid' => $type->id)));
        $this->assertEquals($record->contextid, $instance->contextid);

        // User context.
        $record->contextid = context_user::instance($user->id)->id;
        $instance = $this->getDataGenerator()->create_repository('xpert_flickr_public', $record);

        $this->assertEquals(4, $DB->count_records('repository_instances', array('typeid' => $type->id)));
        $this->assertEquals($record->contextid, $instance->contextid);

        // Invalid context.
        $this->setExpectedException('coding_exception');
        $record->contextid = context_block::instance($block->id)->id;
        $instance = $this->getDataGenerator()->create_repository('xpert_flickr_public', $record);
    }

}
